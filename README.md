# jsau-webapp
[![pipeline status](https://gitlab.sorbonne-paris-nord.fr/11931512/jsau-webapp/badges/master/pipeline.svg)](https://gitlab.sorbonne-paris-nord.fr/11931512/jsau-webapp/-/commits/master)

[![coverage report](https://gitlab.sorbonne-paris-nord.fr/11931512/jsau-webapp/badges/master/coverage.svg)](https://gitlab.sorbonne-paris-nord.fr/11931512/jsau-webapp/-/commits/master)

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
