import axios from 'axios';
export default {


    async get() {
        return (await axios.get("/api/events")).data;
    },

    async getById(id) {
        return (await axios.get(`/api/events/${id}`)).data; 
    },
    create(event) {
        return axios.post('/api/events', event);
    },
    update(id, event) {
        return axios.put(`/api/events/${id}`, event);
    },
    delete(id) {
        return axios.delete(`/api/events/${id}`);
    }

}