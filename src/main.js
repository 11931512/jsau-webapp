import { createApp } from 'vue'
import { createRouter, createWebHashHistory } from 'vue-router'
import App from './App.vue'
import EventsList from './components/EventsList.vue'
import CreateEvent from './components/CreateEvent.vue'
import UpdateEvent from './components/UpdateEvent.vue'



const routes = [
    { path: '/update/:id', component: UpdateEvent },
    { path: '/create', component: CreateEvent },
    { path: '/', component: EventsList },
]

const router = createRouter({
    // 4. Provide the history implementation to use. We are using the hash history for simplicity here.
    history: createWebHashHistory(),
    routes, // short for `routes: routes`
})


const app = createApp(App);
app.use(router);
app.mount("#app");