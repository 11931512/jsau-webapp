
const { mount } = require("@vue/test-utils");
const App = require("../src/App.vue");
describe("Vue Application Test", () => {

    it("create component", () => {

        const wrapper = mount(App);

        // Assert the rendered text of the component
        expect(wrapper.html()).toContain('Vue Events Application')

    })
});